import lcd_i2c as lcd

def print(message,line = 1):
	lcd.lcd_init()
	if line == 1 :
		lcd.lcd_string(message,lcd.LCD_LINE_1)
	else
		lcd.lcd_string(message,lcd.LCD_LINE_2)
		
def clear():
  lcd.lcd_byte(0x01,0)


